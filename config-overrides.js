module.exports = function override(config, env) {
    //do stuff with the webpack config...
    const newConfig = config;

    const isProduction = env === 'production';

    const appVersion = process.env.REACT_APP_VERSION;

    const VERSION = appVersion.replace(/\./g, '');

    const assetUrl = isProduction ? process.env.ASSET_URL : '';

    newConfig.devtool = isProduction ? 'cheap-module-source-map' : 'source-map';
    newConfig.optimization.runtimeChunk = false;

    newConfig.output.filename = isProduction
        ? `static/${VERSION}/js/[name].min.js`
        : 'static/js/[name].[hash].js';
    newConfig.output.chunkFilename = isProduction
        ? `static/${VERSION}/js/chunks/[name].[contenthash:8].js`
        : 'static/js/chunks/[name].[hash].js';

    newConfig.plugins[4].options.filename = `static/${VERSION}/css/[name].min.css`;
    newConfig.plugins[4].options.chunkFilename = `static/${VERSION}/css/chunks/[name].[contenthash:8].css`;

    newConfig.module.rules[1].oneOf[1].options.name = 'static/media/[name].[hash:8].[ext]';
    newConfig.module.rules[1].oneOf[1].options.publicPath = assetUrl;

    newConfig.module.rules[1].oneOf[8].options.name = 'static/media/[name].[hash:8].[ext]';
    newConfig.module.rules[1].oneOf[8].options.publicPath = assetUrl;

    newConfig.optimization.minimizer[0].options.extractComments = false;
    newConfig.optimization.minimizer[0].options
        .terserOptions.compress.drop_console = isProduction;

    return newConfig;
}
